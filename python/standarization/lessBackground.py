#Script to subtract background from video
import cv2
import sys
import argparse
from metadatavideo import metadata_for
from haars import readNumberFrames
import time 
import datetime
def background_substractor(fileName, func = cv2.BackgroundSubtractorMOG()):
    timestamp = metadata_for(fileName)

    if timestamp!=None:
        obj = datetime.datetime.strptime(timestamp['DateCreation'],'%Y-%m-%d %H:%M:%S')
        seconds = time.mktime(obj.timetuple())
    cap = cv2.VideoCapture(fileName)
    numFrames = readNumberFrames(cap)
    print numFrames
    fgbg = func
    frame_count = 0
    counts = []
    while(1):
        ret, frame = cap.read()
        fgmask = fgbg.apply(frame)
        #cv2.imshow('frame',fgmask)
        #k = cv2.waitKey(30)
        j = dict()

        if frame_count % 30 == 0:
            if seconds!=None:
                #UTC time - 5hours
                f = datetime.datetime.fromtimestamp(seconds - 18000).strftime('%Y-%m-%d %H:%M:%S')
                print f
                fileOutputName = "img/noBackground%s.jpg" % f
                j["noBackground"] =fileOutputName
                seconds = seconds + 1
            else:
                fileOutputName = "img/noBackgroundfileName%d.jpg" % frame_count
                j["noBackground"] = fileOutputName

            counts.append(j)
            cv2.imwrite(fileOutputName,fgmask)

        frame_count = frame_count + 1
        if frame_count > numFrames:
            break
    return counts
        #if k == 27:
        #    break
    #cap.release()
    #cv2.destroyAllWindows()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', required=True)
    parser.add_argument('--sub_func', required=False, default=cv2.BackgroundSubtractorMOG())
    args = parser.parse_args(sys.argv[1:])
    return background_substractor(args.file, args.sub_func)
if __name__ == '__main__':
    main()
