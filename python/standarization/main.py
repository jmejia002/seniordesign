import os
import sys
from haars import readVideoInput
from haars import videoProcessing
from lessBackground import background_substractor
sys.path.append('pyMongo/')
import mongoDB

#usage

'''
python resolution.py  fileName  (video)
'''
def mainVideo():

	#width = os.path.expanduser(sys.argv[1])
	#height = os.path.expanduser(sys.argv[2])
	width = 1024;
	height = 1024;
	fileName = os.path.expanduser(sys.argv[1])

	video = readVideoInput(fileName)
	counts = videoProcessing(video = video, width = width, height = height,fileName =fileName)

	noBackgroundCount = background_substractor(fileName = fileName)
	print noBackgroundCount
	i = 0
	for c in noBackgroundCount:
		counts[i]["noBackground"] = c["noBackground"]
		i = i+1
	print counts
	connection = mongoDB.MDB(databaseName = "Crowd", collectionName = "Face-HAARS")
	connection.write(counts)

if __name__ == '__main__':
	mainVideo()