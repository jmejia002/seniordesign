# import the cv2 lib
import cv2
import os
import sys
sys.path.append('pyMongo/')
import mongoDB
from PIL import Image

import time 
import datetime
import re
from metadatavideo import metadata_for
import json

from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
from non_max_suppression_fast import *
 
# construct the argument parse and parse the arguments

#usage

'''
python resolution.py  fileName  (video)
'''
# rotation function where the video input is in portrait mode
def rotateAndScale (img, scaleFactor = 0.5 , degree = 30):
    oldY,oldX, channel = img.shape 
    M = cv2.getRotationMatrix2D(center=(oldX/2,oldY/2), angle=degree, scale=scaleFactor) #rotate about center of image.

    #choose a new image size.
    newX,newY = oldX*scaleFactor,oldY*scaleFactor
    #include this if you want to prevent corners being cut off
    r = np.deg2rad(degree)
    newX,newY = (abs(np.sin(r)*newY) + abs(np.cos(r)*newX),abs(np.sin(r)*newX) + abs(np.cos(r)*newY))

    (tx,ty) = ((newX-oldX)/2,(newY-oldY)/2)
    M[0,2] += tx #third column of matrix holds translation, which takes effect after rotation.
    M[1,2] += ty

    rotatedImg = cv2.warpAffine(img, M, dsize=(int(newX),int(newY)))
    return rotatedImg	
def changeResolution( img,width = 1024, height = 1024):
	image = cv2.resize(img, (height, width), interpolation = cv2.INTER_AREA)
	return image
def readImage(fileName):
	image = cv2.imread(fileName)
	return image
def saveImage(img, fileName):
	fd = cv2.imwrite(fileName, img)
	return

def viewImage(img):
	cv2.imshow("image", img)
	cv2.waitKey(0)
	return
# use for reading the training algorithm for face detection
def readClasifier(fileName):
	image = cv2.CascadeClassifier(fileName)
	return image
def readVideoInput(video_fileName):
	video = cv2.VideoCapture(video_fileName)
	return video
def readFrameRate(video):
	rate = video.get(cv2.cv.CV_CAP_PROP_FPS)
	return rate
def readNumberFrames(video):
	count = video.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
	return count
def currentFrame(video):
	current = video.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
	return current

def detectPedestrian(img):
	img = imutils.resize(img, width=min(800, img.shape[1]))
	hog = cv2.HOGDescriptor()
	hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
	orig = img.copy()
	# detect people in the image
	''' Pedestrians
	(rects, weights) = hog.detectMultiScale(img, winStride=(8, 8),
		padding=(16, 16), scale=1.07 ,  useMeanshiftGrouping= False)

	'''
	(rects, weights) = hog.detectMultiScale(img, winStride=(8, 8),
		padding=(16, 16), scale=1.01 ,  useMeanshiftGrouping= False)
	# draw the original bounding boxes
	for (x, y, w, h) in rects:
		cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
	# apply non-maxima suppression to the bounding boxes using a
	# fairly large overlap threshold to try to maintain overlapping
	# boxes that are still people
	print (rects)
	rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
	pick = non_max_suppression_fast(rects, overlapThresh=0.65)
	print (pick)
	# draw the final bounding boxes
	for (xA, yA, xB, yB) in pick:
		cv2.rectangle(img, (xA, yA), (xB, yB), (0, 255, 255), 2)
	cv2.imshow("Before NMS", orig)
	cv2.imshow("After NMS", img)
	cv2.waitKey(0)
	#print (len(pick))
	saveImage(img, "HOG-MAX.png")
	saveImage(orig, "HOG-ORI.png")
	return len(pick)

def videoProcessing(video, height, width, fileName):
	numFrames = readNumberFrames(video)
	getvalue= currentFrame(video)
	frame_per_second = readFrameRate(video)
	timestamp = metadata_for(fileName)
	counts = []
	if timestamp!=None:
		obj = datetime.datetime.strptime(timestamp['DateCreation'],'%Y-%m-%d %H:%M:%S')
		seconds = time.mktime(obj.timetuple())
		print obj
	while True:
		flag, frame = video.read()
		#viewImage(frame)
		image = changeResolution(img = frame)

		#frame = rotateAndScale(frame,1,90)
		if flag:
			j = dict()
			if seconds!=None:
				#UTC time - 5hours
				f = datetime.datetime.fromtimestamp(seconds - 18000).strftime('%Y-%m-%d %H:%M:%S')
				fileName = "img/HOG%s.jpg" % f
				j["name"] =fileName
				j["time"] = f
			else:
				fileName = "img/HOGfileName%d.jpg" % getvalue
				j["name"] = fileName
				j["time"] = getvalue
			#c = detectPedestrian(img = image)
			j["count"] = 1#c
			j["location"] = "Shepard"
			counts.append(j)
			saveImage(fileName = fileName, img = image)

			getvalue = getvalue + 30;
			seconds = seconds+1;
			video.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,getvalue)
		# 10 sec = 10 * 10000 = 10,000
		else:
			video.set(currentFrame(video), currentFrame(video)-1)
			video.waitKey(1000)
		if(getvalue < numFrames):
			continue
		else:
			#connection = mongoDB.MDB(databaseName = "Crowd", collectionName = "Pedestrian-HOG")
			#connection.write(counts)
			
			with open('HogData.json', 'w') as outfile:
				json.dump(counts, outfile) 
			break



def mainVideo():

	width = 1024;
	height = 1024;
	fileName = os.path.expanduser(sys.argv[1])

	print fileName
	video = readVideoInput(fileName)
	videoProcessing(video = video, width = width, height = height,fileName =fileName)

def mainPicture():
	fileName = os.path.expanduser(sys.argv[1])
	width = 1024
	height = 1024
	image = readImage(fileName)
	detectPedestrian(image)
	#resize_image = changeResolution(image , width = width, height = height)
	#time = 0
	#saveImage(resize_image+time)

if __name__ == '__main__':
	#mainPicture()
	# initialize the HOG descriptor/person detector
	mainVideo()

