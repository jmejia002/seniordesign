# import the cv2 lib
import cv2
import os
import sys

from PIL import Image

import time 
import datetime
import re
from metadatavideo import metadata_for
import json
import numpy as np
from imutils.object_detection import non_max_suppression
from non_max_suppression_fast import *
import imutils

FRONTAL = '../haarcascades/haarcascade_frontalface_default.xml'
FRONTAL_ALT1 = '../haarcascades/haarcascade_frontalface_alt.xml'
FRONTAL_ALT2 = '../haarcascades/haarcascade_frontalface_alt2.xml'
FRONTAL_ALT_TREE = '../haarcascades/haarcascade_frontalface_alt_tree.xml'
PROFILE = '../haarcascades/haarcascade_profileface.xml'
UPPERBODY = '../haarcascades/haarcascade_upperbody.xml'
PROFILE2 = '../haarcascades/lbpcascade_profileface.xml'

# rotation function where the video input is in portrait mode
def rotateAndScale (img, scaleFactor = 0.5 , degree = 30):
    oldY,oldX, channel = img.shape 
    M = cv2.getRotationMatrix2D(center=(oldX/2,oldY/2), angle=degree, scale=scaleFactor) #rotate about center of image.

    #choose a new image size.
    newX,newY = oldX*scaleFactor,oldY*scaleFactor
    #include this if you want to prevent corners being cut off
    r = np.deg2rad(degree)
    newX,newY = (abs(np.sin(r)*newY) + abs(np.cos(r)*newX),abs(np.sin(r)*newX) + abs(np.cos(r)*newY))

    (tx,ty) = ((newX-oldX)/2,(newY-oldY)/2)
    M[0,2] += tx #third column of matrix holds translation, which takes effect after rotation.
    M[1,2] += ty

    rotatedImg = cv2.warpAffine(img, M, dsize=(int(newX),int(newY)))
    return rotatedImg	

def changeResolution( img,width = 1024, height = 1024):

	image = cv2.resize(img, (height, width), interpolation = cv2.INTER_AREA)
	return image

def readImage(fileName):
	image = cv2.imread(fileName)
	return image

def saveImage(img, fileName):
	fd = cv2.imwrite(fileName, img)
	return

def viewImage(img):
	cv2.imshow("image", img)
	cv2.waitKey(0)
	return

# use for reading the training algorithm for face detection
def readClasifier(fileName):
	image = cv2.CascadeClassifier(fileName)
	return image

def readVideoInput(fileName):
	video = cv2.VideoCapture(fileName)
	return video

def readFrameRate(video):
	rate = video.get(cv2.cv.CV_CAP_PROP_FPS)
	return rate

def readNumberFrames(video):
	count = video.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
	return count

def currentFrame(video):
	current = video.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
	return current

def HAARAlgorithm(img,xml):
	faceCascade = cv2.CascadeClassifier(xml)
	faces = faceCascade.detectMultiScale( img,
        	scaleFactor=1.05,
        	minNeighbors=10,
        	minSize=(30, 30), flags=cv2.cv.CV_HAAR_SCALE_IMAGE)
	return faces;

def detectFace(img):

	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	# TODO change this to a class
	# minSize = 20 , 20 it good for mov Feb 29
	orig = img.copy()
	faces1 = (HAARAlgorithm(gray , PROFILE))
	rects1 = np.array([[x, y, x + w, y + h] for (x, y, w, h) in faces1])

	faces2 = (HAARAlgorithm(gray , FRONTAL_ALT2))
	rects2 = np.array([[x, y, x + w, y + h] for (x, y, w, h) in faces2])
	if len(rects1) > 0 and len(rects2) > 0:
		rectsFinal = np.concatenate((rects1, rects2), axis = 0)
	elif len(rects1)> 0:
		rectsFinal = rects1
	else:
		rectsFinal =  rects2 
	for (x, y, w, h) in rectsFinal:
		cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
	pick = non_max_suppression_fast(rectsFinal, overlapThresh=0.5)
	for (xA, yA, xB, yB) in pick:
		cv2.rectangle(img, (xA, yA), (xB, yB), (0, 255, 0), 2)
	cv2.imshow("Before NMS", orig)
	cv2.imshow("After NMS", img)
	saveImage(img, "HAARS-MAX.png")
	saveImage(orig, "HAARS-ORI.png")
	cv2.waitKey(0)
	print len(pick)
	return len(pick)

# returns  a json array with the counts 
def videoProcessing(video, height, width, fileName):
	numFrames = readNumberFrames(video)
	c_frame= currentFrame(video)
	frame_per_second = readFrameRate(video)
	timestamp = metadata_for(fileName)
	counts = []

	if timestamp!=None:
		obj = datetime.datetime.strptime(timestamp['DateCreation'],'%Y-%m-%d %H:%M:%S')
		seconds = time.mktime(obj.timetuple())
	while True:
		flag, frame = video.read()
		#viewImage(frame)
		image = changeResolution(img = frame)

		#frame = rotateAndScale(frame,1,90)
		if flag:
			j = dict()
			if seconds!=None:
				#UTC time - 5hours
				f = datetime.datetime.fromtimestamp(seconds - 18000).strftime('%Y-%m-%d %H:%M:%S')
				fileOutputName = "img/HAARS%s.jpg" % f
				j["haar"] =fileOutputName
				j["time"] = f
				seconds = seconds+1;
			else:
				fileOutputName = "img/HAARSfileName%d.jpg" % c_frame
				j["haar"] = fileOutputName
				j["time"] = c_frame
			c = detectFace(img = image)
			j["count"] = c
			j["location"] = "Shepard"
			counts.append(j)
			saveImage(fileName = fileOutputName, img = image)
			print c_frame
			c_frame = c_frame + 30;

			video.set(cv2.cv.CV_CAP_PROP_POS_FRAMES,c_frame)
		# 10 sec = 10 * 10000 = 10,000
		else:
			video.set(currentFrame(video), currentFrame(video)-1)
			video.waitKey(1000)
		if(c_frame < numFrames):
			continue
		else:
			with open('haarsData.json', 'w') as outfile:
				json.dump(counts, outfile) 
			break
	return counts



def mainPicture():
	fileName = os.path.expanduser(sys.argv[1])
	width = 1024
	height = 1024
	image = readImage(fileName)
	image = changeResolution(img = image)
	detectFace(image)
	#resize_image = changeResolution(image , width = width, height = height)
	#time = 0
	#saveImage(resize_image+time)
def mainVideo():

	width = 1024;
	height = 1024;
	fileName = os.path.expanduser(sys.argv[1])

	print fileName
	video = readVideoInput(fileName)
	videoProcessing(video = video, width = width, height = height,fileName =fileName)

if __name__ == '__main__':
		#mainPicture()
		mainVideo()	

