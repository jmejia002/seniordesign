from hachoir_core.error import HachoirError
from hachoir_core.cmd_line import unicodeFilename
from hachoir_parser import createParser
from hachoir_core.tools import makePrintable
from hachoir_metadata import extractMetadata
from hachoir_core.i18n import getTerminalCharset
import re
import os
import sys

# Get metadata for video file
def metadata_for(filename):

    filename, realname = unicodeFilename(filename), filename
    parser = createParser(filename, realname)
    if not parser:
        print "Unable to parse file"
        exit(1)
    try:
        metadata = extractMetadata(parser)
    except HachoirError, err:
        print "Metadata extraction error: %s" % unicode(err)
        metadata = None
    if not metadata:
        print "Unable to extract metadata"
        exit(1)

    text = metadata.exportPlaintext()
    charset = getTerminalCharset()
    json = {}
    for line in text:
    	creationDate = re.match('(\-\sCreation\sdate)', line)
        #print line
    	#2016-02-29 16:10:26
        
    	if creationDate!=None:
    		date = re.search('(\d+\-\d+\-\d+\s\d+\:\d+\:\d+)',line)
    		if date!=None:
    			creation = {'DateCreation':date.group(0)}
                return creation
        print makePrintable(line, charset)
if __name__ == '__main__': 
    pathname = os.path.expanduser(sys.argv[1])
    meta = metadata_for(pathname)
    
    if meta!=None:
        print meta['DateCreation']
