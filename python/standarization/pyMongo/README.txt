 ### *** README *** ###


### *** Owner *** ###

* JUAN MEJIA
* ROSARION ANTUNEZ
* MICHAEL SAFDIEH

1. Installing MongoDB:

$ brew update
$ brew install mongodb


2. A few things to set up once installed:

$ sudo mkdir -p /data/db

then it will ask you for your password, so type that in
then type:

$ sudo chown <your_mac_username> /data/db

then type your password again
to find out you mac username type:
$ whoami


3. Installing pymongo

$ pip install pymongo


4. Run MongoDB

Open a terminal window and type:
$ mongod
this starts the server

then open a new terminal window and type:
$ mongo
to start running the mongoDB shell (you won't really need to be working here since we are interacting with the DB using Python, but you will need to start the server with the mongod command)


5. Read/Write data to/from db

Please refer to the example file in this directory titled "pymongo_example.py"
In it I provide a simple demonstration of how to use basic interactions between Python and MongoDB


