from pymongo import MongoClient
import json

class MDB:
    def __init__(self, databaseName, collectionName):
        # if __name__ == "__main__":
        self.con = MongoClient() # establishes connection
        self.db = self.con[databaseName] # specifies db
        self.collection = self.db[collectionName] # specifies collection

    def search(self, data=None):
        if data == None:
            return self.collection.find()

    # inserts data to the collection
    def write(self, data):
        self.collection.insert(data)

    # removes collection
    def delete(self):
        document_list = self.collection.find()
        for item in document_list:
            self.collection.remove(item)

    def print_collection(self):
        document_list = self.collection.find()

        for item in document_list:
            print item
    def read(self):
        items = self.collectionName.find()
        return items

# TODO:
# 2. Provide more options for db_search() function



