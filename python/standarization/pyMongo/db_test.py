# this is a test file for the MDB class in mongoDB.py

import mongoDB
import json

# loads json file into the variable "data"
with open("data.json") as json_file:
    json_data = json.load(json_file)
# CONNECTION TEST
test_mdb = mongoDB.MDB(databaseName = "Crowd", collectionName = "Counts")
print "MDB COMPLETE"

# WRITE TEST
test_mdb.write(json_data)
print "write COMPLETE"

# PRINT TEST
test_mdb.print_collection()
print "print COMPLETE"

# QUERY TEST
query_data = list(test_mdb.db_search())
for item in query_data: # this loop is just to show that searching works
    print item
print "search COMPLETE"

# DELETE TEST
'''
test_mdb.db_delete()
print "delete COMPLETE"
'''