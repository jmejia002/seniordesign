# this is following a Youtube tutorial at the following link:
# https://www.youtube.com/watch?v=GSL8JpyAjsE

from pymongo import MongoClient

if __name__ == "__main__":
    con = MongoClient() 

### creates a db within our mongoDB called test_database; if it already exists, it connects to it
db = con.test_database

### creates a collection within our db called people; if it already exists, it connects to it
people = db.people

### inserting data to db
people.insert({'name':'John', 'food':'cheese'})
people.insert({'name':'Jane', 'food':'banana'})
people.insert({'name':'Jack', 'food':'cereal', 'city':'NY'})

### querying all the data in a db
people_list = people.find()

print "INSERT & FIND TEST"
for person in people_list:
    print person

### querying specific data
people_list = people.find({'name':'John'})

print "FIND SPECIFIC INFO"
for person in people_list:
    print person

### updating data
person = people.find_one({'food':'cereal'})
person['food'] = 'egg'
people.save(person)

people_list = people.find({'food':'egg'})

print "UPDATE INFO"
for person in people_list:
    print person


### deleting data
for person in people.find():
    people.remove(person)








