package app.Beans;

import com.mongodb.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by juan on 3/4/16.
 */
@Configuration
public class MongoConnection {

    public @Bean
    MongoTemplate mongoTemplate() throws Exception{
        MongoTemplate mongoTemplate = new MongoTemplate(new MongoClient(),"test");
        return mongoTemplate;
    }

}
