package app.repository;

import app.model.CountModel;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by juan on 3/21/16.
 */
public interface CountRepo extends MongoRepository<CountModel, String> {

}
