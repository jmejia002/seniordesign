package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by juan on 3/2/16.
 */
@SpringBootApplication
public class Application {
    public  static void main (String[] args){
        SpringApplication.run(new Object[]{Application.class},args);
    }
}
