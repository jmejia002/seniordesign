package app.model;

/**
 * Created by juan on 3/21/16.
 */
public class CountModel {

    private String counts;
    private String location;
    private String name;
    private  String time;

    public CountModel(){};

    public CountModel(String counts, String location, String name, String time) {
        this.counts = counts;
        this.location = location;
        this.name = name;
        this.time = time;
    }

    public String getCount() {
        return counts;
    }


    public void setCount(String counts) {
        this.counts = counts;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}



