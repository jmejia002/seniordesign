package app.controller;

import app.model.CountModel;
import app.Beans.MongoConnection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by juan on 3/4/16.
 */
/*
    @class SectionController used for retriving information about each form
 */
    @RestController
public class recordsController{

    ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConnection.class);
    MongoTemplate mongotemplate = (MongoTemplate)ctx.getBean("mongoTemplate");


    @RequestMapping(value ="/count/{location}", method = RequestMethod.GET)
    public ResponseEntity<Map> retrieveCount (
            @PathVariable("location") String location) {
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.put("location", location);

        DBCursor cursor = null;

           cursor = mongotemplate.getCollection("Face-HAARS").find(dbObject);



        List<CountModel> records = new ArrayList<>();
        Map<String , Object> data = new HashMap<>();
        try {

            while (cursor.hasNext()) {
                DBObject object = cursor.next();
                CountModel countModel = new CountModel();

                countModel.setLocation((String) object.get("location"));
                countModel.setName((String) object.get("name"));
                countModel.setCount((String) object.get("counts"));
                records.add(countModel);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }


        data.put("records", records);
        return new ResponseEntity<Map>(data , HttpStatus.OK);

    }
    @RequestMapping(value ="/count/background/{location}", method = RequestMethod.GET)
    public ResponseEntity<Map> background (
            @PathVariable("location") String location) {
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.put("location", location);

        DBCursor cursor = null;

        cursor = mongotemplate.getCollection("noBackground").find(dbObject);



        List<CountModel> records = new ArrayList<>();
        Map<String , Object> data = new HashMap<>();
        try {

            while (cursor.hasNext()) {
                DBObject object = cursor.next();
                CountModel countModel = new CountModel();

                countModel.setLocation((String) object.get("location"));
                countModel.setName((String) object.get("name"));
                countModel.setCount((String) object.get("counts"));
                records.add(countModel);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }


        data.put("records", records);
        return new ResponseEntity<Map>(data , HttpStatus.OK);

    }
}

