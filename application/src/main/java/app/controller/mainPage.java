package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by juan on 3/10/16.
 */
@Controller
public class mainPage {
    @RequestMapping("/home")
    public String home() {
        return "index";
    }
}
