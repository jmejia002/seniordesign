app.controller('homeController', function ($scope,$state,APIService,user,$state) {

console.log('homeController');

$scope.available = false;
$scope.counts = false;
$scope.username = user.get();
$scope.state = "home";
//$scope.locations = ['Shepard',"Other Option"];
$scope.locations= ["Shepard", "NAC", "Library" ];
$scope.toggle = function(type){


	$scope.type = type;
}
$scope.submit = function(selected){
	$scope.available = true;
	APIService.locationAPI($scope.location).then(function(result){

		$scope.data = result.data;
		APIService.locationBackgroundAPI($scope.location).then(function(background){
			for(var i = 0 ; i< $scope.data.records.length; i++){

				for(var j = 0 ; j< background.data.records.length; j++){

							var original = $scope.data.records[i].name.split("img/HAARS")[1]
							var back  = background.data.records[j].name.split("img/noBackground")[1]
						if (original == back) {

							$scope.data.records[i].background = background.data.records[j].name;
							

						};


				}
			}
	})
	})

}

$scope.Counts = function(){
	$scope.counts = true;
}

$scope.logout = function(){
	user.clear();
	$state.go("login")
}

});