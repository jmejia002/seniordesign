/*

@factory {user} : factory for storing the information of the user  for caching 
*/


app.factory('user', function  () {
	// body...

	var userdata = '';

	var functions = {};
	/*
	@function {set} function to set the user
	@param {string} a string for setting the user
	*/
	functions.set = function(user){
		userdata = user;
	}
	/*
	@function {get} function to get the current  user
	@param {} the function does not take any arguments
	*/
	functions.get = function(){
		return userdata;
	}
	functions.clear = function(){
		userdata = ''
	}

	return functions;
});



app.factory('data', function(){

	var data = '';
	var functions = {};

	functions.set = function  (input) {
		data = input;
	}
	functions.get = function(){
		return data;
	}

	return functions;




})