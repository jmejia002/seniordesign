app.service('APIService', function($http){

	var self  = this;


    self.locationAPI = function(location,type){
        var result = $http({
            method:'GET',
            url: "http://localhost:8080/count/"+location
        }).success(
            function(data, status, headers,config)
            {
                return data;
            });
        return result;
    };
    self.locationBackgroundAPI = function(location){
        var result = $http({
            method:'GET',
            url: "http://localhost:8080/count/background/"+location
        }).success(
            function(data, status, headers,config)
            {
                return data;
            });
        return result;
    };
})