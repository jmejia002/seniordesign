

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
 
    $stateProvider
        .state('login', {
            url:'/',
            templateUrl: 'views/login.html',
            controller: 'login'
        })
        .state('about', {
            url:'/about',
            templateUrl: 'views/about.html',
            controller: 'aboutController'
        })
        .state('home', {
            url:'/home',
            templateUrl: 'views/home.html',
            controller: 'homeController'
        })
        .state('register',{
            url:'/register',
            templateUrl:'views/register.html',
            controller:'registerController'
        })
        .state('welcome', {
            url:'/welcome',
            templateUrl:'views/welcome.html',
            controller: 'welcomeController'
        })
        .state('contact', {
            url:'/contact',
            templateUrl:'views/contact.html',
            controller: 'contactController'
        })

 
}]);