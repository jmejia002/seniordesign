# Readme Crowd Counting V1.0
* Juan Mejia
* Rosario Antunez
*  Michael Safdieh
##### This document describes how this application can be run and what dependencies are needed.
## 1. Folders
This application has three major folders
##### 1.1 Application
It  contains the backend for our application and makes connections to a mongodb database for writing and retrieving data to show it to in the frontend.
```
├── application
     ├── pom.xml
     └── src
      └──  main
      ├── java
            │   └── app
            │       ├── Application.java
            │       ├── Beans
            │       ├── controller
            │       ├── model
            │       └── repository
            └── resources
```
##### 1.2 Python
This folder contains the algorithms used for counting people in a video or picture
```sh
python/
├── haarcascades
└── standarization
    ├── img
    ├── pedestrians
    └── pyMongo
```
#### 1.3 Static
This folder  contains the front end  for the application where the the counts of each picture are displayed
```sh
static
├── assets
│   └── img
│       └── backgrounds
├── components
├── img
├── scripts
│   ├── controllers
│   ├── factory
│   └── services
├── styles
└── views
```

### 2. Installation
```sh
$ git clone  https://jmejia002@bitbucket.org/jmejia002/seniordesign.git
```
#### 2.1 Runnig Front End
- The front end can run in an Apache server pointing to the static folder or for convinience an local nodejs server. This application was design using a nodejs server.
##### 2.1.2 Dependencies
Dependencies are managed using npm and bower. We assume you have npm, gulp and bower install in your system.
```sh
# install node packages
$ npm install 
$ cd static
# install jquery, angularjs, etc
$ bower install bower.json
$ cd ..
# run the front end server
$ gulp connect
```
* After all the dependencies are installed the server will start.
For convience, No account is needed for using the application. Please enter any "username" and "password" for entering to the home page. This is due because, It requires a mysql dabatabase for registering users, but for the purpose of this documents that dependency is left out. However this can be overwritting in a fully developed application.

#### 2.2 Backend
-   The backend  can run as a jar application or using maven which embeds a tomcat server.
```sh
# using maven
$ cd application
$ mvn spring-boot:run
```
```sh
# using the jar file.
seniorDesign:$ java -jar mongoRest-1.0-SNAPSHOT.jar
```
#### 2.3 Database
In order to use crowd counting MongoDB is used as database.Please install MongoDB is not installed.
* The following are the steps for saving data to the database
```sh
seniorDesing$ cd python/standarization/dataForDataBase
$ sudo mongoimport --host=127.0.0.1 --db test --collection Face-HAARS --file data.json --jsonArray
$ sudo mongoimport --host=127.0.0.1 --db test --collection noBackground 
```
### 3. Python 
##### 3.1 Dependencies
Crowd  Counts uses OpenCv for image processing. 
```sh
$ pip install http://bitbucket.org/haypo/hachoir/wiki/hachoir-metadata
$ pip install hachoir-parser  
$ pip install hachoir-core
$ pip install pymongo
```
##### 3.2 Running Algorithms
```sh
$ seniordesign/python/standarization/
# Hog algorithm, which produces json file HogData.json with counts
$ python hog.py namevideo
# Haars algorithm, which produces json file haarsData.json with counts
$ python haars.py namevideo
```
## 4. Appendix
If any questions, please contact the contributors to this project
* Juan: jua.n46k@gmail.com
*  Rosario: mrantunez@gmail.com
*  Michael: mikesafdieh@gmail.com


